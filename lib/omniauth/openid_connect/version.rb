# frozen_string_literal: true

module OmniAuth
  module OpenIDConnect
    VERSION = '0.10.0'
  end
end
